if __name__ == '__main__':
    names = []
    f = open('p022_names.txt')
    names = sorted(f.read().replace('"', '').split(','), key=str)

    result = 0
    counter = 1
    for i in names:
        worth = 0
        for j in i:
            worth += ord(j)-64

        result += counter * worth
        counter += 1
    print(result)