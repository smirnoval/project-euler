if __name__ == '__main__':
    dividers = []
    for i in range(2, 10000):
        result = 0
        for j in range(1,i):
            if i % j == 0 and i != j:
                result += j
        dividers.append(result)

    a = 2
    result = 0
    for i in dividers:
        if i < 3 or i >= 10000:
            a += 1
            continue
        temp = dividers[i-2]
        if i == temp:
            a += 1
            continue
        if a == temp:
            print(a, i, temp)
            result += a
        a += 1

    print("Answer:", result)