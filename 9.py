#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
from itertools import product

if __name__ == '__main__':
    for x, y in product(range(1,1000), range(1,1000)):
        z = math.sqrt(x*x+y*y)
        if z % 1 == 0.0:
            if x + y + z == 1000:
                print(int(x*y*z))
                break