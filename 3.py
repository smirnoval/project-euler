#!/usr/bin/env python
# -*- coding: utf-8 -*-

def sieve_of_erastosthenes(n):
    sieve = list(range(n + 1))
    sieve[1] = 0
    for i in sieve:
        if i > 1:
            for j in range(i + i, len(sieve), i):
                sieve[j] = 0
    return sieve

if __name__ == '__main__':
    number = 600851475143
    amount_prime_numbers = 50000
    dividers =list(filter(lambda x: x!=0 and number % x == 0 ,sieve_of_erastosthenes(amount_prime_numbers)))
    print(dividers.pop())
