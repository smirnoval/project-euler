def factorial(n):
    result = 1
    for i in range(1,n+1):
        result *= i
    return result

if __name__ == '__main__':
    answer = factorial(100)
    answer = str(answer)
    result = 0
    for i in answer:
        result += int(i)
    print(result)