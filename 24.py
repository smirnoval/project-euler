import itertools

if __name__ == '__main__':
    n = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    a = itertools.permutations(n)
    b = []
    for i in a:
        b.append(i)
    for i in b[999999]:
        print(i, end="")
    print(" <- Answer")
