def fermats_cycle(n, d):
    for i in range(1, d):
        if 1 == 10**i % d:
            return i
    return 0

if __name__ == '__main__':
    num = []
    for i in range(2,1001):
        num.append(fermats_cycle(1, i))
    print(max(num))
    print([i for i in range(2,1001) if fermats_cycle(1, i) == max(num)])