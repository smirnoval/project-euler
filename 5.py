#!/usr/bin/env python
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    answer = 0
    for x in range(21, 1000000000):
        flag = True
        for y in range(1,21):
            if x % y == 0:
                continue
            else:
                flag = False
                break
        if flag == False:
            continue
        else:
            answer = x
            break
    print(answer)
