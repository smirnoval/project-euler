#!/usr/bin/env python
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    palindrome = 0
    pal_flag = False
    for x in range(100,1000):
        for y in range(100, 1000):
            n = x * y
            pal_flag = True
            for z in range(len(str(n)) // 2):
                if str(n)[z] == str(n)[-(z+1)]:
                    continue
                else:
                    pal_flag = False
                    break
            if pal_flag == True and n > palindrome:
                palindrome = n
    print(palindrome)
