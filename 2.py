#!/usr/bin/env python
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    summary = 0
    first = 1
    second = 2
    while (second < 4000000):
        if second % 2 == 0:
            summary += second
        temp = second
        second += first
        first = temp
    print(summary)