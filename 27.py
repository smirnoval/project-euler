import math

def is_prime(n):
    if n <= 1: return False
    if n <= 3: return True
    if n%2==0 or n%3 == 0: return False
    r = int(math.sqrt(n))
    f = 5
    while f <= r:
        if n%f == 0 or n%(f+2) == 0: return False
        f+= 6
    return True

def sieve_of_erastosthenes(n):
    sieve = list(range(n + 1))
    sieve[1] = 0
    for i in sieve:
        if i > 1:
            for j in range(i + i, len(sieve), i):
                sieve[j] = 0
    return sieve


if __name__ == '__main__':
    L = 1000
    dividers =list(filter(lambda x: x!=0, sieve_of_erastosthenes(L)))
    nmax = 0
    for b in dividers:
        for a in range(-b, b, 2):
            n = 1
            while is_prime(n*n + a*n + b): n += 1
            if n > nmax: nmax, p = n, a * b

    print("Project Euler 27 Solution = ", p, "Sequence length =", nmax)