#!/usr/bin/env python
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    summary = 0
    for i in range(1, 1000):
        if (i % 3 == 0) or (i % 5 == 0):
            summary += i
            continue
        else:
            continue
    print(summary)