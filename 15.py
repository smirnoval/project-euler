def grid(n):
    L = []
    for i in range(n):
        x = []
        for j in range(n):
            x.append(0)
        L.append(x)
    for i in range(2,n+2):
        L[0][i-2] = i
        L[i-2][0] = i
    for i in range(1,n):
        for j in range(1,n):
            L[i][j] = L[i-1][j] + L[i][j-1]
    return L[n-1][n-1]

if __name__ == '__main__':
    answer = grid(20)
    print(answer)
