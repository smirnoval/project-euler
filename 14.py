if __name__ == '__main__':
    answer = 0
    ans_counter = 0
    for i in range(1,1000000):
        y = i
        counter = 0
        while i != 1:
            if i % 2 == 0:
                i = i / 2
            else:
                i = 3*i + 1
            counter += 1
        if ans_counter < counter:
            ans_counter = counter
            answer = y
    print(answer, ans_counter)

