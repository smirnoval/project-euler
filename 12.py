import math

def divisors(n):
    z = 0
    for i in range(1, int(math.ceil(math.sqrt(n)))):
        if n % i == 0:
            z += 2
        else:
            continue
    return z

if __name__ == '__main__':
    x=1
    for y in range(2,1000000):
        x += y
        if divisors(x) >= 500:
            print(x)
            break